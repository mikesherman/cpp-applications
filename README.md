# C++ Applications
A repository for the various programs I have written in C++
- **Data structures**: Implementation of 3 common data structures
- **Euclidean algorithm**: Computes the greatest common divisor of two integers using the Extended Euclidean Algorithm
- **Ice cream inventory**: Inventory application for an ice cream stand
- **Longest common subsequence**: Uses a grid to calculate the longest common subsequence between two strings
- **Matrix chains**: Finds the most efficient way to multiply a sequence of matrices
- **Modular exponentiation**: description
- **Monte Carlo pi**: Approximating pi using the "Monte Carlo" method
- **Parser**: description
- **Sample bank software**: Software system of a large bank
- **Scheduler**: Simulates an OS's scheduler using 3 different algorithms
- **Sorting algorithms**: Implementation of 4 common sorting algorithms
- **Two largest**: description