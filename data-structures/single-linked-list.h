#include <stddef.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

struct node {
    int data;
    struct node* next;
    struct node* start;
};

class SingleLinkedList {
    public:
    node *getNode() {
        node* newNode;
        newNode = (node*)malloc(sizeof(node));
        cout << "Enter data: ";
        int temp;
        cin >> temp;
        newNode->data = temp;
        newNode->next = NULL;
        return newNode;
    }
    void createList(int n) {
        int i;
        node *newNode;
        node *temp;
        for (i = 0; i < n; i++) {
            newNode = getNode();
            if (start == NULL) {
                start = newNode;
            }
            else {
                temp = start;
                while (temp->next != NULL)
                    temp = temp->next;
                temp->next = newNode;
            }
        } 
    }
    void displayList() {
        node *temp;
        temp = start;
        cout << "The contents of your list from left to right:\n";
        if (start == NULL) {
            cout << "Empty list.";
        }
        else {
            while (temp != NULL) {
                cout << temp->data << " ";
                temp = temp->next;
            }// end while
            cout << endl;
            system("pause");
        }
    }
    int countNode() {
        node *temp = start;
        int result = 0;

        while (temp != NULL) {
            result += 1;
            temp = temp->next;
        }

        return result;
    }
    int countNode2() {
        int ctr;
        node *X = start;
        for (ctr = 1; X != NULL; ctr++)
        {
            X = X->next
        }
        return ctr;
    }
    int countNode3() {
        int ctr;
        node *X = start;
        for (ctr = 1; X->next != NULL; ctr++)
        {
            X = X->next
        }
        return ctr;
    }
    void deleteAtMid() {
        int ctr = 1;
        int pos, nodeCtr;
        node *temp, *prev;
        if (start == NULL) {
            cout << "Empty list.\n";
            return;
        }
        else {
            cout << "Enter position of the node you wish to delete: ";
            cin >> pos;
            nodeCtr = countNode();
            if (pos > 1 && pos < nodeCtr) {
                temp = prev = start;
                while (ctr < pos) {
                    prev = temp;
                    temp = temp->next;
                    ctr++;
                }//end while
                prev->next = temp->next;
                free(temp);
                cout << "Node deleted.\n";
            }//end if
            else {
                cout << "Invalid position.\nIt may be out of the bounds of the list or not in the middle.\n";
            }//end else
        }//end else
    }
    void insertAtMid() {
        node *newNode, *temp, *prev;
        int pos, nodeCtr;
        int ctr = 1;
        newNode = getNode();
        cout << "Enter the position where you would like to insert the node: ";
        cin >> pos;
        nodeCtr = countNode();
        if (pos > 1 && pos < nodeCtr) {
            temp = prev = start;
            while (ctr < pos) {
                prev = temp;
                temp = temp->next;
                ctr++;
            }//end while
            prev->next = newNode;
            newNode->next = temp;
        }
        else {
            cout << "Invalid position.\nIt may be out of the bounds of the list or not in the middle.\n";
        }
    }
    private:
    node *start = NULL;
};









