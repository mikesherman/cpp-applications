
#include <stddef.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

void mainMenu {
    bool exit = false;
	int choice, n;
	while (exit == false) {
		system("CLS");
		cout << "What would you like to do?\n"
			<< "---------------------------------\n"
			<< "1. Create a new single linked list\n"
			<< "2. Create a new double linked list\n"
			<< "3. Create a new queue\n"
			<< "4. Display list\n"
			<< "5. Insert a node in the beginning\n"
			<< "6. Insert a node into the middle\n"
            << "7. Insert a node at the end\n"
            << "8. Delete a node from the beginning)\n"
            << "9. Delete a node from the middle\n"
            << "10. Delete a node from the end\n"
            << "11. Exit program\n"
			<< "---------------------------------\n"
			<< "Enter choice: ";
		cin >> choice;
		switch (choice) {
		case 1:
			cout << "How many items in your list? ";
			cin >> n;
			createList(n);
			cout << "Your list has been created.\n";
			system("pause");
			break;
		case 2:
			displayList();
			break;
		case 3:
			deleteAtEnd();
			system("pause");
			break;
		case 4:
			insertAtBeginning();
			system("pause");
			break;
		case 7:
			exit = true;
			break;
		default:
			cout << "Invalid choice. Enter a number from 1 to 11.\n";
			system("pause");
		}//end switch
	}//end main while
	return 0;
}

int q_main()
{
	int choice;
	while (1) {
		system("CLS");
		choice = mainMenu();
		switch (choice) {
		case 1:
			enqueue();
			system("pause");
			break;
		case 2:
			dequeue();
			system("pause");
			break;
		case 3:
			display();
			system("pause");
			break;
		case 4:
			return 0;
		default:
			cout << "Invalid choice. Enter a number from 1 to 4.\n";
			system("pause");
		}//end switch
	}
}
