#include <stddef.h>
#include <stdlib.h>
#include <iostream>


using namespace std;

struct node {
	int data;
	struct node* left;
	struct node* right;
};

node* start = NULL;

node* setNode() {
	node* newNode;
	newNode = (node*)malloc(sizeof(node));
	cout << "Enter data: ";
	int temp;
	cin >> temp;
	newNode->data = temp;
	newNode->left = NULL;
	newNode->right = NULL;
	return newNode;
}

void createList(int n) {
	int i;
	node *newNode;
	node *temp;
	for (i = 0; i < n; i++) {
		newNode = setNode();
		if (start == NULL) {
			start = newNode;
		}
		else {
			temp = start;
			while (temp->right)
				temp = temp->right;
			temp->right = newNode;
			newNode->left = temp;
		}
	} 
}

void displayList() {
	node *temp;
	temp = start;
	cout << "The contents of your list from left to right:\n";
	if (start == NULL) {
		cout << "Empty list.";
	}
	else {
		while (temp != NULL) {
			cout << temp->data << " ";
			temp = temp->right;
		}// end while
		cout << endl;
		system("pause");
	}
}

int countNode() {
	node *temp = start;
	int result = 0;
	
	while (temp != NULL) {
		result += 1;
		temp = temp->right;
	}
	
	return result;
}

void deleteAtEnd() {
	node *temp;
	if (start == NULL) {
		cout << "Empty list.\n";
	}
	else {
		temp = start;
		while (temp->right) {
			temp = temp->right;
		}
		temp->left->right = NULL;
		free(temp);
	}
}

void insertAtBeginning() {
	node *newNode;
	newNode = setNode();
	if (start == NULL) {
		start = newNode;
	}
	else {
		newNode->right = start;
		start->left = newNode;
		start = newNode;
	}
}